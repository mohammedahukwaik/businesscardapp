import 'package:flutter/material.dart';

void main()
{
  runApp( const BusinessCardApp());
  print('mohammed adel');
  print('mohammed adel');
}

class BusinessCardApp extends StatelessWidget {
  const BusinessCardApp({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
        home: Scaffold(
        backgroundColor: const Color(0xFF2B475E),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const [
            CircleAvatar(
              radius: 101,
              backgroundColor: Colors.white,
              child: CircleAvatar(
                radius: 100,
                backgroundImage: AssetImage('images/mohammed.png'),
              ),
            ),
            Text(
                'Mohammed Abu Kwaik',
              style: TextStyle(
                color: Colors.white,
                fontSize: 32,
                fontFamily: 'Pacifico',
              ),
            ),
            Text(
                'FLUTTER DEVELOPER',
              style: TextStyle(
                color: Color(0xFF6C8090),
                fontSize: 22,
                fontWeight: FontWeight.bold,
              ),
            ),
            Divider(
              color: Color(0xFF6C8090),
              thickness: 1,
              indent: 20,
              endIndent: 20,
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: ListTile(
                leading: Icon(
                  Icons.mail,
                  size: 35,
                  color: Color(0xFF2B475E),
                ),
                title: Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(
                    'mohammedahukwaik@gmail.com',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xFF2B475E),
                      fontFamily: 'Pacifico',
                    ),
                  ),
                ),
              ),
            ),
            Card(
              margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: ListTile(
                leading: Icon(
                  Icons.facebook,
                  size: 35,
                  color: Color(0xFF2B475E),
                ),
                title: Padding(
                  padding: EdgeInsets.only(left: 0.0),
                  child: Text(
                    'Mohammed Adel Abu Kwaik',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: Color(0xFF2B475E),
                      fontFamily: 'Pacifico',
                    ),
                  ),
                ),
              ),
            ),
            Card(
                margin: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
                child: ListTile(
                  leading: Icon(
                    Icons.phone,
                    size: 35,
                    color: Color(0xFF2B475E),
                  ),
                  title: Padding(
                    padding: EdgeInsets.only(left: 0.0),
                    child: Text(
                      '0799157985',
                      style: TextStyle(
                        fontSize: 24,
                        color: Color(0xFF2B475E),
                        fontFamily: 'Pacifico',
                      ),
                    ),
                  ),
                )
            ),
          ],
        ),
      ),
    );
  }
}
